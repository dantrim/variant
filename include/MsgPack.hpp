#ifndef __MSG_PACK_HPP__
#define __MSG_PACK_HPP__

#include <variant.hpp>
#include <cstdlib>
#include <stdint.h>
#include <iostream>
#include <vector>
#include <typeinfo>
#include <unordered_map>

#include <MemoryBuffer.hpp>

namespace msgpack {
  constexpr uint8_t FIXMAP=0x80;
  constexpr uint8_t FIXARRAY=0x90;
  constexpr uint8_t FIXSTR=0xa0;
  constexpr uint8_t NIL=0xc0;
  constexpr uint8_t NEVER_USED=0xc1;
  constexpr uint8_t FALSE=0xc2;
  constexpr uint8_t TRUE=0xc3;
  constexpr uint8_t BIN_8=0xc4;
  constexpr uint8_t BIN_16=0xc5;
  constexpr uint8_t BIN_32=0xc6;
  constexpr uint8_t EXT_8=0xc7;
  constexpr uint8_t EXT_16=0xc8;
  constexpr uint8_t EXT_32=0xc9;
  constexpr uint8_t FLOAT_32=0xca;
  constexpr uint8_t FLOAT_64=0xcb;
  constexpr uint8_t UINT_8=0xcc;
  constexpr uint8_t UINT_16=0xcd;
  constexpr uint8_t UINT_32=0xce;
  constexpr uint8_t UINT_64=0xcf;
  constexpr uint8_t INT_8=0xd0;
  constexpr uint8_t INT_16=0xd1;
  constexpr uint8_t INT_32=0xd2;
  constexpr uint8_t INT_64=0xd3;
  constexpr uint8_t FIXEXT_1=0xd4;
  constexpr uint8_t FIXEXT_2=0xd5;
  constexpr uint8_t FIXEXT_4=0xd6;
  constexpr uint8_t FIXEXT_8=0xd7;
  constexpr uint8_t FIXEXT_16=0xd8;
  constexpr uint8_t STR_8=0xd9;
  constexpr uint8_t STR_16=0xda;
  constexpr uint8_t STR_32=0xdb;
  constexpr uint8_t ARRAY_16=0xdc;
  constexpr uint8_t ARRAY_32=0xdd;
  constexpr uint8_t MAP_16=0xde;
  constexpr uint8_t MAP_32=0xdf;

  constexpr uint32_t CONV_NONE=0x00;
  constexpr uint32_t CONV_UINT_8_16=0x01;
  constexpr uint32_t CONV_UINT_8_32=0x02;
  constexpr uint32_t CONV_UINT_8_64=0x04;
  constexpr uint32_t CONV_UINT_16_32=0x08;
  constexpr uint32_t CONV_UINT_16_64=0x10;
  constexpr uint32_t CONV_UINT_32_64=0x20;
  constexpr uint32_t CONV_INT_8_16=0x100;
  constexpr uint32_t CONV_INT_8_32=0x200;
  constexpr uint32_t CONV_INT_8_64=0x400;
  constexpr uint32_t CONV_INT_16_32=0x800;
  constexpr uint32_t CONV_INT_16_64=0x1000;
  constexpr uint32_t CONV_INT_32_64=0x2000;
  constexpr uint32_t CONV_FLOAT32_64=0x10000;

  static std::unordered_map<uint8_t,std::size_t> typesize_map={
    { FLOAT_32,sizeof(float)   },
    { FLOAT_64,sizeof(double)  }, 
    { UINT_8  ,sizeof(uint8_t)   },
    { UINT_16 ,sizeof(uint16_t) },
    { UINT_32 ,sizeof(uint32_t) },
    { UINT_64 ,sizeof(uint64_t) },
    { INT_8   ,sizeof(int8_t)      },
    { INT_16  ,sizeof(int16_t)   },
    { INT_32  ,sizeof(int32_t)   },
    { INT_64  ,sizeof(int64_t)   }
  };

 static std::unordered_map<std::type_index, uint8_t> typeid_map={
    { typeid(float) , FLOAT_32 },
    { typeid(double) , FLOAT_64 }, 
    { typeid(uint8_t) , UINT_8 },
    { typeid(uint16_t) , UINT_16 },
    { typeid(uint32_t) , UINT_32 },
    { typeid(uint64_t) , UINT_64 },
    { typeid(int8_t),  INT_8 },
    { typeid(int16_t) , INT_16 },
    { typeid(int32_t) , INT_32 },
    { typeid(int64_t) , INT_64 }
  };

  

  template <unsigned  N>
  class Reader {  
    using V=variant_impl::variant<N>;
  public:
    Reader(MemoryReadBuffer &mem,V &var,bool toDouble=false): _toDouble(toDouble) ,_mem(mem),_var(var) {
      readToken(_var);      
    }
  private:
    bool _toDouble;
    void readToken(V &v) {
      uint8_t token;
 
      _mem.get(token);
      switch(token) {
      case NIL: readNull(v); break;
      case NEVER_USED: throw("MsgPack: Invalid token"); break;// never used  
      case TRUE:  
      case FALSE: readBool(v,token); break; //false  
      case BIN_8: readBin<uint8_t>(); break;
      case BIN_16: readBin<uint16_t>(); break;
      case BIN_32: readBin<uint32_t>(); break;
      case EXT_8: readExt<uint8_t>(); break;
      case EXT_16: readExt<uint16_t>(); break;
      case EXT_32: readExt<uint32_t>(); break;
      case FLOAT_32: 
	if constexpr (N==64) {
	    if(_toDouble)
	      readValue<float,double>(v);
	    else
	      readValue<float>(v);
	  } 
	else
	  readValue<float>(v); 
	break;
      case FLOAT_64: 
	if constexpr (N==64)
	readValue<double>(v); 
	else throw("MsgPack: FLOAT_64 not supported");
	break;
      case UINT_8: readValue<uint8_t>(v); break;
      case UINT_16: readValue<uint16_t>(v); break;
      case UINT_32: readValue<uint32_t>(v); break;
      case UINT_64: 
	if constexpr (N==64)
		       readValue<uint64_t>(v);
	else throw("MsgPack: UINT_64 not supported");
	break;
      case INT_8: readValue<int8_t>(v);break;
      case INT_16:readValue<int16_t>(v);break;
      case INT_32: readValue<int32_t>(v);break;	
      case INT_64: 
	if constexpr (N==64)
	readValue<int64_t>(v);
	else throw("MsgPack: INT_64 not supported");
	break;
      case FIXEXT_1: readFixExt<1>();break;
      case FIXEXT_2: readFixExt<2>();break;  
      case FIXEXT_4: readFixExt<6>();break;   
      case FIXEXT_8: readFixExt<8>();break;  
      case FIXEXT_16: readFixExt<16>();break;    
      case STR_8: readStr<uint8_t>(v);break;
      case STR_16: readStr<uint16_t>(v);break;
      case STR_32: readStr<uint32_t>(v);break;  
      case ARRAY_16: readArray<uint16_t>(v); break;
      case ARRAY_32: readArray<uint32_t>(v); break;  
      case MAP_16: readMap<uint16_t>(v);break;
      case MAP_32: readMap<uint32_t>(v);break;  
	
      default: 
	uint8_t tmp=token&0x80;
	if(tmp==0x00) readPosFixInt(v,token&0x7f);
	tmp=token&0xe0;
	if(tmp==0xe0) readNegFixInt(v,token&0x1f);
	if(tmp==0xa0) readStr<uint8_t>(v,token&0x1f,false);
	tmp=token&0xf0;
	if(tmp==0x80) readMap<uint8_t>(v,token&0xf,false);
	if(tmp==0x90) readArray<uint8_t>(v,token&0xf,false);    
	break;
      }
    }
    
    template<typename T,typename U=T> void readValue(V &v) {
      T val;
      _mem.get(val);
      v=U(val);
    }
    
    template<typename T> void readStr(V &v,uint8_t token=0,bool getToken=true) {
      v=readString<T>(token,getToken);
    }

    std::string readKey() {
      uint8_t token;
      _mem.get(token);
      std::string result;
      switch(token) {
      case 0xd9: return readString<uint8_t>();break;
      case 0xda: return readString<uint16_t>();break;
      case 0xdb: return readString<uint32_t>();break;  
      default:
	if((token&0xe0)==0xa0) return readString<uint8_t>(token&0x1f,false);	
	break;
      }
      throw("MsgPack: invalid string key");
      return std::string(""); // never reached;
	
    }
    
    template<typename T> std::string readString(uint8_t token=0,bool getToken=true) {
      T len;
      if(getToken) _mem.get(len); else len=token;
      if(len==0) return std::string("");     
      std::string result(_mem.getBytes(len),len);
      return result;
    }
    
    
    void readBool(V &v,uint8_t val) {
      bool b=(val==0xc3); 
      v=b;
    }
    void readPosFixInt(V &v,uint8_t val) {
      v=val;      
    }
    void readNegFixInt(V &v,uint8_t val) {
      v=int8_t(val);
    }
    
    void readNull(V &v) {
      v=V();
    }
    
    // byte array
    template<typename T> void readBin() {
      T len;
      _mem.get(len);
      std::vector<uint8_t> result(len);
      const uint8_t *p=(const uint8_t*) _mem.getBytes(len);
      for(std::size_t i=0;i<len;i++) result[i]=p[i];    
    }
    template <unsigned U> void readFixExt() {
      uint8_t type;
      std::size_t len=U;
      _mem.get(type);
      _mem.getBytes(len);  
    // handle types  
    }
    template <typename T> void readExt() {
      uint8_t type;
      T len;
      _mem.get(len);
    _mem.get(type);
    _mem.getBytes(len);  
    // handle types  
    }
    template <typename T> void readMap(V &v,uint8_t token=0,bool getToken=true) {
      T len;
      V result;
      if(getToken)  _mem.get(len);
      else len=token;
      for(std::size_t i=0;i<len;i++) {
	std::string key=readKey();
	readToken(result[key]);
      }
      v=result;
    }

    bool readTypedArray(V &var,std::size_t len) {
      uint8_t last=0;
      uint8_t tok=0;
      _mem.peek(tok,0);
      if(tok>=FLOAT_32 && tok<=INT_64) {
	std::size_t sz=typesize_map[tok];
	if constexpr (N==32)
		       if(sz>4) throw("MsgPack: 64-bit typed arrays not suppoted");
	std::size_t offset=(len-1)*(1+sz);
	_mem.peek(last,offset);
	if(tok!=last) return false;
      }
      
      switch(tok) {
      case UINT_8: return readTypedArray<uint8_t>(var,len); break;
      case UINT_16: return readTypedArray<uint16_t>(var,len); break;
      case UINT_32: return readTypedArray<uint32_t>(var,len); break;
      case UINT_64:
	if constexpr (N==64)
		       return readTypedArray<uint64_t>(var,len); 
	else throw("MsgPack: unsupported variant");
	break;	
      case INT_8: return readTypedArray<int8_t>(var,len); break;
      case INT_16: return readTypedArray<int16_t>(var,len); break;
      case INT_32: return readTypedArray<int32_t>(var,len); break;
      case INT_64: 
	if constexpr (N==64)
		       return readTypedArray<int64_t>(var,len); 
	else throw("MsgPack: unsupported variant");;
	break;	
      case FLOAT_32: 
	if constexpr (N==64) {
	if(_toDouble) return  readTypedArray<float,double>(var,len);
	  else return readTypedArray<float>(var,len); 
	  }
	else
	  return readTypedArray<float>(var,len); 
	break;
      case FLOAT_64: 
	if constexpr (N==64)
		       return readTypedArray<double>(var,len); 
	else throw("MsgPack: unsupported variant");;
	break;
      default:
	break;
      }
      return false;
    }

    template <typename T,typename U=T> bool readTypedArray(V &var,std::size_t len) {
      std::vector<U> result;      
      //      std::size_t pos=_mem.pos();
      uint8_t token=typeid_map[typeid(T)];
      std::size_t sz=typesize_map[token];
      std::size_t offset=0;
      for(std::size_t i=0;i<len;i++) {
	uint8_t tkn;
	T val;
     	bool ok=_mem.peek(tkn,offset) && _mem.peek(val,offset+1);	
	if(!ok) {
	  return false;	  
	}
	offset+=sz+1;
	result.push_back(val);
      }
      var=result;
      _mem.advance(offset);
      return true;
    }

    template <typename T> void readArray(V &v,uint8_t token=0, bool getToken=true) {
      v=V(V::Type::array);
      //      V result(V::Type::array);
      T len;
      if(getToken) _mem.get(len); else len=token;
      bool is_typed_array=readTypedArray(v,len);      
      if(!is_typed_array)  {
	v=V(V::Type::array);
	for(std::size_t i=0;i<len;i++) {
	  V el;
	  readToken(el);
	  v.push_back(el);
	}
      }
    }
 
    
    MemoryReadBuffer &_mem;
    V &_var;
  };
  template <unsigned N>
  class Writer {
    using V=variant_impl::variant<N>;
  public:
    Writer(MemoryWriteBuffer &mem,const V &var):_mem(mem),_var(var) {
      writeValue(var);
    }

    void _writeArrayHeader(std::size_t length)  const  {
      uint64_t n=bit_width(length);
      if(n>32) throw("MsgPack: unsupported array size");
      else if(n>16) {_mem.put(uint8_t(0xdd));  _mem.put(uint32_t(length)); } 
      else if(n>4)  {_mem.put(uint8_t(0xdc));  _mem.put(uint16_t(length)); }
      else _mem.put(uint8_t(0x90|length));
    }
    
    
    template <typename T> void writeTypedArray(const V &var) const {
      const std::vector<T> &ar=var;
      _writeArrayHeader(ar.size());
      for(auto const &a:ar) {
	_mem.put(typeid_map[typeid(T)]);
	_mem.put(a);
      }
    }

    void writeArray(const typename V::array_t &ar) {
      _writeArrayHeader(ar.size());
      for(auto const &a:ar) writeValue(a);
    }

    void writeObject(const typename V::object_t &obj) {
      uint64_t len=obj.size();
      uint64_t n=bit_width(len);
      if(n>32) throw("MsgPack: unsupported object size");
      else if(n>16) {_mem.put(uint8_t(0xdf));  _mem.put(uint32_t(len)); } 
      else if(n>4)  {_mem.put(uint8_t(0xde));  _mem.put(uint16_t(len)); }
      else _mem.put(uint8_t(0x80|len));
      for(auto const &o:obj) {writeStr(o.first) ; writeValue(o.second);}
    }
    void writeStr(const std::string &str) {
      uint64_t len=str.size();
      uint64_t n=bit_width(len);
      if(n>32) throw("MsgPack: unsupported  string size");
      else if(n>16) {_mem.put(uint8_t(0xdb));  _mem.put(uint32_t(len)); } 
      else if(n>8) {_mem.put(uint8_t(0xda));  _mem.put(uint16_t(len)); } 
      else if(n>5) {_mem.put(uint8_t(0xd9));  _mem.put(uint8_t(len)); } 
      else  _mem.put(uint8_t(0xa0|len));
      _mem.putBytes((const char*) str.data(),str.size());
    }

    template <typename T> void writeScalar(const V &v) {
      T val=std::get<T>(v.value);
      _mem.put(typeid_map[typeid(T)]);
      _mem.put(val);
    }
    void writeUint8(const V &v) {      
      uint8_t val=std::get<uint8_t>(v.value);
      if(val>127)
	_mem.put(typeid_map[typeid(uint8_t)]);
      _mem.put(val);
    }
    void writeInt8(const V &v) {      
      uint8_t val=std::get<int8_t>(v.value);
      if((int8_t) val<-32)
	_mem.put(typeid_map[typeid(int8_t)]);
      _mem.put(val);
    }
    
    void writeBool(const V &v) {
      bool b=std::get<bool>(v.value);
      uint8_t op=FALSE;
      if(b) op=TRUE;
      _mem.put(op);
    }
    void writeNull() { _mem.put(NIL);}

    void writeValue(const V &v) {
      if(v.is_object()) writeObject(*std::get<typename V::ObjPtr>(v.value));
      else if(v.is_array()) writeArray(*std::get<typename V::ArrayPtr>(v.value));
      else if(v.is_string()) writeStr(*std::get<typename V::StrPtr>(v.value));
      else if(v.is_boolean()) writeBool(v);
      else if(v.is_null()) writeNull();
      else switch(v.index()) {	  
	case V::Type::uint64: 
	  if constexpr (N==64)
			 writeScalar<uint64_t>(v); 
	  else throw("MsgPack: UINT64 is unsupported");
	  break;
	case V::Type::uint32: writeScalar<uint32_t>(v); break;  
	case V::Type::uint16: writeScalar<uint16_t>(v); break;    
	case V::Type::uint8:  writeUint8(v); break; 
	case V::Type::int64: 
	  if constexpr (N==64)
			 writeScalar<int64_t>(v); 
	  else throw("MsgPack: INT64 is unsupported");
	  break;
	case V::Type::int32: writeScalar<int32_t>(v); break;  
	case V::Type::int16: writeScalar<int16_t>(v); break;    
	case V::Type::int8: writeInt8(v); break; 
    	case V::Type::float32: writeScalar<float>(v); break;    
	case V::Type::float64: 
	  if constexpr (N==64)
			 writeScalar<double>(v); 
	  else throw("MsgPack: FLOAT64 is unsupported");
	  break;
	case V::Type::array_uint64: 
	  if constexpr (N==64)
			 writeTypedArray<uint64_t>(v); 
	  else throw("MsgPack: ARRAY_UINT64 is unsupported");
	  break;
	case V::Type::array_uint32: writeTypedArray<uint32_t>(v); break;  
	case V::Type::array_uint16: writeTypedArray<uint16_t>(v); break;    
	case V::Type::array_uint8:  writeTypedArray<uint8_t>(v); break; 
	case V::Type::array_int64: 
	  if constexpr (N==64)			 
			 writeTypedArray<int64_t>(v); 
	  else throw("MsgPack: ARRAY_INT64 is unsupported");
	  break;	  
	case V::Type::array_int32: writeTypedArray<int32_t>(v); break;  
	case V::Type::array_int16: writeTypedArray<int16_t>(v); break;    
	case V::Type::array_int8: writeTypedArray<int8_t>(v); break; 
    	case V::Type::array_float: writeTypedArray<float>(v); break;	  
	case V::Type::array_double: 
	  if constexpr (N==64)
			 writeTypedArray<double>(v); 
	  else throw("MsgPack: ARRAY_FLOAT64 is unsupported");
	  break;
	  default:
      throw;
      break;
	}    
    }
    
  private:
    MemoryWriteBuffer &_mem;
    const V &_var;  
#if  defined(__PPC__) || defined(__arm__)    
    inline uint32_t bit_width(uint32_t arg) const {
      uint32_t count=0;
      for(unsigned i=sizeof(arg)*8;i>0;i--) {
	if((arg&(1<<(i-1)))==0) count++;      
	else break;
      }
      return  sizeof(arg)*8-count;
    }
#else
    inline uint32_t bit_width(uint64_t arg) const {
      if (arg==0) return 1; 
      return sizeof(arg)*8 - __builtin_clzl(arg);
    }
#endif
  };
  using Writer64=Writer<64>;
  using Writer32=Writer<32>;
  using Reader64=Reader<64>;
  using Reader32=Reader<32>;
}









#endif





